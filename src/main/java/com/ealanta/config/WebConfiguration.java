package com.ealanta.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.ealanta.hateoas.JsonRootRelProvider;

@Configuration
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public RelProvider relProvider() {
		return new JsonRootRelProvider();
	}
	

}
