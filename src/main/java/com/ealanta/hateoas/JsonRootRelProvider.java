package com.ealanta.hateoas;

import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.core.DefaultRelProvider;
import org.springframework.plugin.core.Plugin;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonRootName;

public class JsonRootRelProvider implements RelProvider {

	Class<Plugin> clazz = org.springframework.plugin.core.Plugin.class;
	
    private DefaultRelProvider defaultRelProvider = new DefaultRelProvider();

    @Override
	public String getCollectionResourceRelFor(Class<?> type) {
        JsonRootName rootNameAnn = type.getAnnotationsByType(JsonRootName.class)[0];
        String result;
        String rootName = rootNameAnn.value();
        if(StringUtils.isEmpty(rootName) == false){
        	result = rootName + "s";
        }else{
        	return defaultRelProvider.getCollectionResourceRelFor(type);
        }
        return result;
	}

	@Override
	public String getItemResourceRelFor(Class<?> type) {
        JsonRootName rootNameAnn = type.getAnnotationsByType(JsonRootName.class)[0];
        String rootName = rootNameAnn.value();
        String result;
        if(StringUtils.isEmpty(rootName) == false){
        	result = rootName;
        }else{
        	result = defaultRelProvider.getCollectionResourceRelFor(type);
        }
        return result;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return defaultRelProvider.supports(clazz);
	}


}
