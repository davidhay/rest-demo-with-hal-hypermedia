package com.ealanta.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author davidhay
 */
@Converter(autoApply=true)
public class BooleanConverter implements AttributeConverter<Boolean, String>{

	@Override
	public String convertToDatabaseColumn(Boolean bool) {
		String result = Boolean.TRUE.equals(bool) ? "Y" : "N";
		return result;
	}

	@Override
	public Boolean convertToEntityAttribute(String value) {
		boolean result = "Y".equalsIgnoreCase(value);
		return result;
	}

}
