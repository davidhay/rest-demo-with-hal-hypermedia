package com.ealanta.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author davidhay
 */
@Entity
@Table(name="user_task")
@JsonRootName("task")
public class Task extends AbstractBaseEntity {
	
	private static final long serialVersionUID = 1L;

	@Column(length=80)
	@Size(min=4,max=80)
	private String description;
	
	@Column(length=1)
	private Boolean completed;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate;
	
	@Enumerated(EnumType.STRING)
	private Priority priority;
	
	//@ManyToOne(fetch=FetchType.EAGER)
	//@JoinColumn(name="user_id",nullable=false,referencedColumnName="id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private User user;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss'Z'", timezone="GMT")
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	
}
