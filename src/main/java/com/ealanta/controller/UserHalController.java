package com.ealanta.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ealanta.domain.Task;
import com.ealanta.domain.User;
import com.ealanta.repository.TaskRepository;
import com.ealanta.repository.UserRepository;


@RestController
@RequestMapping("/hal/users")
public class UserHalController {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private TaskRepository taskRepo;


	@RequestMapping(method=RequestMethod.GET)
	public Resources<Resource<User>> getUsers() {
		Link selfLink = getUsersLink().withSelfRel();
		List<Resource<User>> users =  StreamSupport.stream(userRepo.findAll().spliterator(), false)
				.map(user -> getUserResource(user))
				.collect(Collectors.toList());
		Resources<Resource<User>> result = new Resources<>(users);
		result.add(selfLink);
		return result;
	}
	
	private Resource<User> getUserResource(User user){
		Link selfLink = getUserLink(user.getId()).withSelfRel();
		Link taskLink = getUserTasksLink(user.getId()).withRel("tasks");
		Resource<User> resource = new Resource<User>(user,selfLink, taskLink);
		return resource;
	}
	
	private Resource<Task> getTaskResource(Task task){
		Link selfLink = getUserTaskLink(task.getUser().getId(),task.getId()).withSelfRel();
		Link taskLink = getUserLink(task.getUser().getId()).withRel("user");
		Resource<Task> resource = new Resource<Task>(task,selfLink, taskLink);
		return resource;
	}
	
	private ControllerLinkBuilder getUserLink(Long userId) {
		return linkTo(methodOn(UserHalController.class).getUser(userId));
	}
	private ControllerLinkBuilder getUsersLink() {
		return linkTo(methodOn(UserHalController.class).getUsers());
	}
	private ControllerLinkBuilder getUserTasksLink(Long userId) {
		return linkTo(methodOn(UserHalController.class).getUserTasks(userId));
	}
	private ControllerLinkBuilder getUserTaskLink(Long userId, Long taskId) {
		return linkTo(methodOn(UserHalController.class).getUserTask(userId,taskId));
	}
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public Resource<User> getUser(@PathVariable("userId") Long userId) {
		User user = userRepo.findOne(userId);
		Resource<User> res = getUserResource(user);
		return res;
	}

	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.GET)
	public Resources<Resource<Task>> getUserTasks(@PathVariable("userId") Long userId) {

		List<Task> data = taskRepo.findByUserId(userId);
		Link selfLink = getUserTasksLink(userId).withSelfRel();
		List<Resource<Task>> tasks =  StreamSupport.stream(data.spliterator(), false)
				.map(task -> getTaskResource(task))
				.collect(Collectors.toList());
		Resources<Resource<Task>> result = new Resources<>(tasks);
		result.add(selfLink);
		return result;
		
	}

	@RequestMapping(value="/{userId}/tasks/{taskId}", method=RequestMethod.GET)
	public Resource<Task> getUserTask(@PathVariable("userId") Long userId, @PathVariable("taskId") Long taskId) {

		Task task = taskRepo.findOne(taskId);
		Assert.isTrue(userId.equals(task.getUser().getId()));
		
		Resource<Task> res = getTaskResource(task);
		return res;
	}
	
	
	
	@RequestMapping(value="/{userId}/tasks",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Resource<Task>> createUserTask(@PathVariable("userId") Long userId, 
			@RequestBody Task task) {
		Assert.isNull(task.getId());
		Assert.isNull(task.getUser());
		User user = userRepo.findOne(userId);
		task.setUser(user);
		Task saved = taskRepo.save(task);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(getUserTaskLink(user.getId(),saved.getId()).toUri());
		return new ResponseEntity<Resource<Task>>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Resource<User>> createUser(@RequestBody User user) throws IOException {
		Assert.isNull(user.getId());
		User saved = userRepo.save(user);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(getUserLink(saved.getId()).toUri());
		return new ResponseEntity<Resource<User>>(headers, HttpStatus.CREATED);
	}

}
