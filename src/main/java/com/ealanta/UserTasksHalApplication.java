package com.ealanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserTasksHalApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserTasksHalApplication.class, args);
	}
}
