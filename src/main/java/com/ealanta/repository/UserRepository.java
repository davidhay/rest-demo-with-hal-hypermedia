package com.ealanta.repository;

import org.springframework.data.repository.CrudRepository;

import com.ealanta.domain.User;

public interface UserRepository extends CrudRepository<User, Long>{

	public User findByUsernameAllIgnoringCase(String username);
}
