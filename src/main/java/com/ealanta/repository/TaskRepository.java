package com.ealanta.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ealanta.domain.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {

	public List<Task> findByUserId(Long userId);

}
