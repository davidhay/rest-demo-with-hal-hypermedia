package com.ealanta.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ealanta.UserTasksHalApplication;

/**
 * @author davidhay
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksHalApplication.class)
@WebAppConfiguration
public class RestUserTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void testGetUsers() throws Exception {
		mockMvc.perform(get("/hal/users"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/hal/users"))))
		.andExpect(jsonPath("$._embedded.users", hasSize(2)))
		.andExpect(jsonPath("$._embedded.users[0].firstname", is(equalTo("Joe"))))
		.andExpect(jsonPath("$._embedded.users[0].lastname", is(equalTo("Bloggs"))))
		.andExpect(jsonPath("$._embedded.users[0].username", is(equalTo("joebloggs"))))
		.andExpect(jsonPath("$._embedded.users[0]._links.self.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(jsonPath("$._embedded.users[0]._links.tasks.href", is(equalTo("http://localhost/hal/users/1/tasks"))))
		.andExpect(jsonPath("$._embedded.users[1].firstname", is(equalTo("Mollie"))))
		.andExpect(jsonPath("$._embedded.users[1].lastname", is(equalTo("Malone"))))
		.andExpect(jsonPath("$._embedded.users[1].username", is(equalTo("molliem"))))
		.andExpect(jsonPath("$._embedded.users[1]._links.self.href",  is(equalTo("http://localhost/hal/users/2"))))
		.andExpect(jsonPath("$._embedded.users[1]._links.tasks.href", is(equalTo("http://localhost/hal/users/2/tasks"))))
		.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1() throws Exception {
		mockMvc.perform(get("/hal/users/1"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.firstname", is(equalTo("Joe"))))
		.andExpect(jsonPath("$.lastname", is(equalTo("Bloggs"))))
		.andExpect(jsonPath("$.username", is(equalTo("joebloggs"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(jsonPath("$._links.tasks.href", is(equalTo("http://localhost/hal/users/1/tasks"))))
		.andExpect(status().isOk());
	}
	@Test
	public void testGetUser2() throws Exception {
		mockMvc.perform(get("/hal/users/2"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.firstname", is(equalTo("Mollie"))))
		.andExpect(jsonPath("$.lastname", is(equalTo("Malone"))))
		.andExpect(jsonPath("$.username", is(equalTo("molliem"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/2"))))
		.andExpect(jsonPath("$._links.tasks.href", is(equalTo("http://localhost/hal/users/2/tasks"))))
		.andExpect(status().isOk());
	}
	@Test
	public void testGetUser1Tasks() throws Exception {
		mockMvc.perform(get("/hal/users/1/tasks"))
		.andDo(print())
		//11
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/hal/users/1/tasks"))))
		.andExpect(jsonPath("$._embedded.tasks", hasSize(3)))
		.andExpect(jsonPath("$._embedded.tasks[0].description", is(equalTo("task eleven"))))
		.andExpect(jsonPath("$._embedded.tasks[0].completed", is(equalTo(false))))
		.andExpect(jsonPath("$._embedded.tasks[0].dueDate", is(equalTo("2016-01-01T01:02:03Z"))))
		.andExpect(jsonPath("$._embedded.tasks[0].priority", is(equalTo("LO"))))
		.andExpect(jsonPath("$._embedded.tasks[0]._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/11"))))
		.andExpect(jsonPath("$._embedded.tasks[0]._links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		//12
		.andExpect(jsonPath("$._embedded.tasks[1].description", is(equalTo("task twelve"))))
		.andExpect(jsonPath("$._embedded.tasks[1].completed", is(equalTo(true))))
		.andExpect(jsonPath("$._embedded.tasks[1].dueDate", is(equalTo("2016-01-02T02:03:04Z"))))
		.andExpect(jsonPath("$._embedded.tasks[1].priority", is(equalTo("MED"))))
		.andExpect(jsonPath("$._embedded.tasks[1]._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/12"))))
		.andExpect(jsonPath("$._embedded.tasks[1]._links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		//13
		.andExpect(jsonPath("$._embedded.tasks[2].description", is(equalTo("task thirteen"))))
		.andExpect(jsonPath("$._embedded.tasks[2].completed", is(equalTo(false))))
		.andExpect(jsonPath("$._embedded.tasks[2].dueDate", is(equalTo("2016-01-03T03:04:05Z"))))
		.andExpect(jsonPath("$._embedded.tasks[2].priority", is(equalTo("HI"))))
		.andExpect(jsonPath("$._embedded.tasks[2]._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/13"))))
		.andExpect(jsonPath("$._embedded.tasks[2]._links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUser1Task11() throws Exception {
		mockMvc.perform(get("/hal/users/1/tasks/11"))
		.andDo(print())
		//11
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/hal/users/1/tasks/11"))))
		.andExpect(jsonPath("$.description", is(equalTo("task eleven"))))
		.andExpect(jsonPath("$.completed", is(equalTo(false))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2016-01-01T01:02:03Z"))))
		.andExpect(jsonPath("$.priority", is(equalTo("LO"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/11"))))
		.andExpect(jsonPath("$._links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUser1Task12() throws Exception {
		mockMvc.perform(get("/hal/users/1/tasks/12"))
		.andDo(print())
		//12
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.description", is(equalTo("task twelve"))))
		.andExpect(jsonPath("$.completed", is(equalTo(true))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2016-01-02T02:03:04Z"))))
		.andExpect(jsonPath("$.priority", is(equalTo("MED"))))
		.andExpect(jsonPath("$_links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/12"))))
		.andExpect(jsonPath("$_links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUser1Task13() throws Exception {
		mockMvc.perform(get("/hal/users/1/tasks/13"))
		.andDo(print())
		//13
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.description", is(equalTo("task thirteen"))))
		.andExpect(jsonPath("$.completed", is(equalTo(false))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2016-01-03T03:04:05Z"))))
		.andExpect(jsonPath("$.priority", is(equalTo("HI"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/13"))))
		.andExpect(jsonPath("$._links.user.href",  is(equalTo("http://localhost/hal/users/1"))))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testCreateUser() throws Exception {
		String userJson = "{\"firstname\" : \"Saint\",\"lastname\" : \"Nicholas\", \"username\" : \"santa\"}";
		mockMvc.perform(post("/hal/users/")
				.contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
		.andDo(print())
		.andExpect(header().string("Location", is(equalTo("http://localhost/hal/users/3"))))
		.andExpect(status().isCreated());
		
		mockMvc.perform(get("/hal/users/3"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.firstname", is(equalTo("Saint"))))
		.andExpect(jsonPath("$.lastname", is(equalTo("Nicholas"))))
		.andExpect(jsonPath("$.username", is(equalTo("santa"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/3"))))
		.andExpect(jsonPath("$._links.tasks.href", is(equalTo("http://localhost/hal/users/3/tasks"))))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testCreateUserTask() throws Exception {
		String userTaskJson = "{\"description\":\"desc\",\"completed\":false,\"dueDate\":\"2015-12-31T23:00:00Z\",\"priority\":\"HI\"}";
		mockMvc.perform(post("/hal/users/1/tasks")
				.contentType(MediaType.APPLICATION_JSON)
                .content(userTaskJson))
		.andDo(print())
		.andExpect(header().string("Location", is(equalTo("http://localhost/hal/users/1/tasks/24"))))
		.andExpect(status().isCreated());
		
		mockMvc.perform(get("/hal/users/1/tasks/24"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.description", is(equalTo("desc"))))
		.andExpect(jsonPath("$.completed", is(equalTo(false))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2015-12-31T23:00:00Z"))))
		.andExpect(jsonPath("$.priority", is(equalTo("HI"))))
		.andExpect(jsonPath("$._links.self.href",  is(equalTo("http://localhost/hal/users/1/tasks/24"))))
		.andExpect(status().isOk());
				
	}
	
	@Test
	public void testGetUser2Task21() throws Exception {
		mockMvc.perform(get("/hal/users/2/tasks/21"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.description", is(equalTo("task twenty one"))))
		.andExpect(jsonPath("$.completed",is(equalTo(false))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2016-01-04T04:05:06Z"))))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/hal/users/2/tasks/21"))))
		.andExpect(jsonPath("$._links.user.href", is(equalTo("http://localhost/hal/users/2"))))
		.andExpect(status().isOk());
	}

}
