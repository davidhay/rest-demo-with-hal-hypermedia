# Rest Demo with Spring Boot / Spring MVC / Spring Data and  Spring HATEOAS

This Java Application also creates a Rest API for Users/Tasks. This app uses SpringBoot/SpringMVC/SpringHATEOAS and SpringData with HSQLDB.
The rest endpoints have integration tests written using Spring's Mock MVC framework.

## REST URLS FOR READ/GET ##

* http://localhost:8080/hal/users
* http://localhost:8080/hal/users/{userid}
* http://localhost:8080/hal/users/{userid}/tasks
* http://localhost:8080/hal/users/{userid}/tasks/{taskid}

## REST URLS POST/CREATE ##

to create a new user 

post to http://localhost:8080/hal/users

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"firstname" : "Saint","lastname" : "Nicholas", "username" : "Santa"}' http://localhost:8080/hal/users

HTTP/1.1 201 Created

Location: http://localhost:8080/hal/users/4
```

to create a new user task

post to http://localhost:8080/hal/users/1/tasks

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"dueDate":"2017-04-01T01:02:03Z" , "priority":"MED" , "completed":false, "description": "a new task"}' http://localhost:8080/hal/users/1/tasks

HTTP/1.1 201 Created

Location: http://localhost:8080/hal/users/1/tasks/24
```

To run the java app


```
#!python

$ mvn spring-boot:run
```